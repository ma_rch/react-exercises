import React from "react";
import { useSelector, useDispatch } from "react-redux";
import CardsGallery from "../../components/CardsList/CardsGallery.jsx";
import Button from "../../components/button/index.jsx";
import checkModalContent from "../../components/modal/checkModalContent.js";

const Favorit = () => {
    const dispatch = useDispatch();
    const favoritCards = useSelector((state) => state.favoritCardsReducer.favoritCards);
    const anyCardInFavorit = favoritCards[0];

    return (
        <>
            {favoritCards.length ? (
                <>
                    <div style={{ marginLeft: "10px", marginTop: "10px" }}>
                        <Button
                            text={"Clear All"}
                            bg={"bisque"}
                            color={"brown"}
                            onClick={(e) => {
                                checkModalContent(e, anyCardInFavorit, dispatch);
                            }}
                            assignment={"clearAllFavorits"}
                        ></Button>
                    </div>
                    <CardsGallery
                        btnAssignment={"modalAddToCart"}
                        btnText={"add to Cart"}
                        cards={favoritCards}
                    ></CardsGallery>
                </>
            ) : (
                <h2 className="h2Title" style={{ textAlign: "center", fontStyle: "italic" }}>
                    В избранном пока что ничего нет
                </h2>
            )}
        </>
    );
};

export default Favorit;
