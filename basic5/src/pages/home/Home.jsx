import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCardsMiddleware } from "../../redux/middlewares/getCardsMiddleware";
import CardsGallery from "../../components/CardsList/CardsGallery";

const Home = () => {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getCardsMiddleware());
    }, [dispatch]);
    const allCards = useSelector((state) => state.getCardsReducer.cards);
    if (allCards.length === 0) {
        return <h2 style={{ textAlign: "center", fontSize: "48px" }}>Loading...</h2>;
    }
    return (
        <>
            <CardsGallery
                btnAssignment={"modalAddToCart"}
                btnText={"add to Cart"}
                cards={allCards}
            ></CardsGallery>
        </>
    );
};

export default Home;
