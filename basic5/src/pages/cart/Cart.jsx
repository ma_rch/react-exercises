import React from "react";
import { useSelector, useDispatch } from "react-redux";
import Button from "../../components/button/index.jsx";
import CardsGallery from "../../components/CardsList/CardsGallery.jsx";
import checkModalContent from "../../components/modal/checkModalContent.js";
import FormOrder from "../../components/form/FormOrder.jsx";
import s from "./cart.module.scss";
import { messageOrderSuccessAction } from "../../redux/actions/orderSuccessAction.js";

const Cart = () => {
    const dispatch = useDispatch();
    const succeessOrderMessage = useSelector(
        (state) => state.messageOrderSuccessReducer.isVisibleMessage
    );
    const cart = useSelector((state) => state.cartCardsReducer.cartCards);
    const anyCardInCart = cart[0];
    return (
        <>
            {succeessOrderMessage && (
                <div className={s.successMessageBlock}>
                    <h2 className={s.successMessage}>Заказ Успешно Сформирован</h2>
                    <Button
                        text={"OK"}
                        onClick={() => dispatch(messageOrderSuccessAction(false))}
                        bg={"rgb(5, 147, 5)"}
                        color={"burlywood"}
                    />
                </div>
            )}
            {cart.length ? (
                <>
                    <div className={s.btnGroup}>
                        <Button
                            text={"Clear All"}
                            bg={"bisque"}
                            color={"brown"}
                            onClick={(e) => {
                                checkModalContent(e, anyCardInCart, dispatch);
                            }}
                            assignment={"clearAllCart"}
                        ></Button>
                    </div>
                    <CardsGallery
                        cards={cart}
                        btnAssignment={"removeFromCart"}
                        btnText={"remove from Cart"}
                    />
                    <FormOrder />
                </>
            ) : (
                <h2 className="h2Title" style={{ textAlign: "center", fontStyle: "italic" }}>
                    В корзине пока что ничего нет
                </h2>
            )}
        </>
    );
};

export default Cart;
