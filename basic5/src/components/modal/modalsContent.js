export function modalsContent() {
    return [
        {
            id: "modalAddToCart",
            headerText: "Подтверждение действия",
            bodyText: "Вы действительно хотите добавить этот товар в корзину ?",
        },

        {
            id: "removeFromCart",
            headerText: "Подтверждение действия",
            bodyText: "Вы действительно хотите удалить этот товар из корзины ?",
        },

        {
            id: "clearAllFavorits",
            headerText: "Подтверждение действия",
            bodyText: "Вы действительно хотите очистить весь список избранного ?",
        },
        {
            id: "clearAllCart",
            headerText: "Подтверждение действия",
            bodyText: "Вы действительно хотите очистить вcю корзину ?",
        },
        {
            id: "confirmOrderForm",
            headerText: "Подтверждение заказа",
            bodyText:
                "заказ будет сформирован и отправлен по указанному адресу. Подтверждаете заказ ?",
        },
    ];
}

export function findModalContent(btnAssign, cart, selectedCard) {
    let findModalContent = modalsContent().find((modal) => modal.id === btnAssign);
    findModalContent = findModalContent
        ? findModalContent
        : {
              headerText: "Error",
              bodyText:
                  "no data, try later, please close this modal window - click any button in modal window",
          };
    if (
        cart.find(
            (el) => el.articul === selectedCard.articul && findModalContent.id === "modalAddToCart"
        )
    ) {
        findModalContent.bodyText = "такой товар в корзине уже есть, хотите добавить еще один ?";
    }
    if (findModalContent.headerText === "Error" && btnAssign === "pay") {
        findModalContent.bodyText =
            "this is only a tutorial site, if you really want to purchase this item, please visit the original site: https://privilegehandmade.com/";
    }
    return findModalContent;
}
