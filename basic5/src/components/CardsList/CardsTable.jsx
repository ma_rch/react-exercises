// этот компонент не используется - загатовка для basic6

const CardsTable = ({ cards }) => {
    return (
        <table>
            <thead>
                <tr>
                    <th>articul</th>
                    <th>name</th>
                    <th>price</th>
                    <th>color</th>
                    <th>img</th>
                </tr>
            </thead>
            <tbody>
                {cards.map((card) => (
                    <tr key={card.articul}>
                        <td>{card.articul}</td>
                        <td>{card.name}</td>
                        <td>{card.price}</td>
                        <td>{card.color}</td>
                        <td>
                            <img src={card.img} alt={card.name} width={120} />
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
    );
};

export default CardsTable;
