import React from "react";
import { Routes, Route } from "react-router-dom";
import Header from "./components/header/Header.jsx";
import { Cart, Favorit, Home, NotFound } from "./pages/index.js";
// import Modal from "./components/modal/Modal.jsx";

const App = () => {
    return (
        <>
            <Header></Header>
            <Routes>
                <Route path="/" element={<Home />}></Route>
                <Route path="/favorit" element={<Favorit />}></Route>
                <Route path="/cart" element={<Cart />}></Route>
                <Route path="*" element={<NotFound />}></Route>
            </Routes>
            {/* <Modal></Modal> */}
        </>
    );
};

export default App;
