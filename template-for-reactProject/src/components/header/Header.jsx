// import { useSelector } from "react-redux";
import { NavLink, Link } from "react-router-dom";
import s from "./header.module.scss";
// import Star from "../components/Star/Star.jsx";

function Header(props) {
    return (
        <header className={s.header}>
            <Link to="/">
                <h3 className={s.logo}>
                    Presents <span className={s.logo2}>Shop</span>{" "}
                </h3>
            </Link>
            <nav className={s.nav}>
                <ul className={s["nav-list"]}>
                    <li className={s["nav-item"]}>
                        <NavLink to="/">Home</NavLink>
                    </li>
                    <li className={s["nav-item"]}>
                        <NavLink to="/favorit">Favorit</NavLink>
                    </li>
                    <li className={s["nav-item"]}>
                        <NavLink to="/cart">Cart</NavLink>
                    </li>
                </ul>
            </nav>
            <div className={s.wrapper}>
                {/* <div className={s.star}>
                    <Link to="/favorit">
                        <Star fill={"gold"} />
                    </Link>
                    <span>{favorit.length}</span>
                </div> */}

                <div className={s.cart}>
                    <Link to="/cart">
                        <img src="./img/shopping-cart.png" alt="" width={28} height={28} />
                    </Link>
                    {/* <span>{cartCounter}</span> */}
                </div>
            </div>
        </header>
    );
}

export default Header;
