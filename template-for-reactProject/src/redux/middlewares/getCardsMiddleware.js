import { getCardsAction } from "../actions/getCardsAction";

export const getCardsMiddleware = () => (dispatch) =>
    fetch("./catalog.json")
        .then((response) => response.json())
        .then((data) => dispatch(getCardsAction(data)));
