export const getCardsAction = (cards) => {
    return {
        type: "GET_CARDS_SUCCESS",
        payload: cards,
    };
};
