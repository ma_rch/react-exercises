import { combineReducers } from "redux";
// import { modalReducer } from "./redusers/modalReducer.js";
// import { favoritReducer } from "./redusers/favoritReducer.js";
// import { selectedCardReducer } from "./redusers/SelectedCardReducer.js";
// import { cartReducer } from "./redusers/cartReducer.js";
import getCardsReducer from "./getCardsReducer";

const rootReducer = combineReducers({
    getCardsReducer,
});

export default rootReducer;
