const initialState = {
    cards: [],
};

const getCardsReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_CARDS_SUCCESS":
            return {
                ...state,
                cards: action.payload,
            };
        default:
            return state;
    }
};

export default getCardsReducer;
