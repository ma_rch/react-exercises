import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCardsMiddleware } from "../../redux/middlewares/getCardsMiddleware";
// import CardsList from "../../components/CardsList/CardsList";

const Home = (props) => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getCardsMiddleware());
    }, [dispatch]);

    const cards = useSelector((state) => state.getCardsReducer.cards);
    console.log(cards);
    if (cards.length === 0) {
        return <div>Loading...</div>;
    }
    return (
        <>
            {cards.map((card) => {
                return (
                    <div key={card.articul}>
                        <h3>{card.name}</h3>
                    </div>
                );
            })}
        </>
    );
};

export default Home;
