import { Component } from "react";
import Actions from "../components/actions";
import s from "./home.module.scss";
import Modal from "../components/modal";
import { textContent, title } from "./content";

export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = { active: false, id: null };
    }

    handleClick = (e) => {
        if (e && e.target.tagName === "BUTTON" && e.target.id) {
            this.setState({ active: !this.state.active, id: e.target.id });
        } else {
            this.setState({ active: !this.state.active });
        }
    };

    render() {
        return (
            <>
                <h1 style={{ textAlign: "center" }}>{title}</h1>
                <p>{textContent}</p>
                <Actions
                    extraClass={s["wrapper-btn"]}
                    btn1Text="Open first modal"
                    btn1Bg="red"
                    btn2Text="Open second modal"
                    btn2Bg="green"
                    onClick1={this.handleClick}
                    onClick2={this.handleClick}
                    btn1id="deleteConfirm1"
                    btn2id="goodIdea1"
                />
                {/* {this.state.active && ( <Modal... 
                    да модалку надо рендерить только если тру, но тогда пропадает анимация ! 
                    потому что когда модалка есть изначально в ДОМ то за счет транизшн и опасити происходит плавный 
                    переход от опасити 0 к 1 и анимация работает но если элемент из ничего появляется в ДОМ то анимация 
                    не срабатывает. Как это исправить ? я не знаю поэтому рендерю модалку сразу просто скрываю ее*/}

                <Modal
                    active={this.state.active}
                    setActive={this.handleClick}
                    okSomeLogic={this.handleClick}
                    btnId={this.state.id}
                />
            </>
        );
    }
}
