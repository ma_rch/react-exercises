import { Component } from "react";
import s from "./button.module.scss";

export default class Button extends Component {
    render() {
        const { text, bg, onClick, id } = this.props;
        return (
            <button id={id} className={s.btn} style={{ backgroundColor: bg }} onClick={onClick}>
                {text}
            </button>
        );
    }
}
