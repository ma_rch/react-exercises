import { Component } from "react";
import s from "./modal.module.scss";
import Actions from "../actions";

import { modalsContent } from "./modalContent.js";

export default class Modal extends Component {
    // constructor(props) {
    //     super(props);
    // }

    render() {
        const { active, setActive, okSomeLogic, btnId } = this.props;
        let findModalContent = modalsContent.find((modal) => modal.id === btnId);
        findModalContent = findModalContent
            ? findModalContent
            : {
                  headerText: "Error",
                  closeButton: false,
                  text: "no data, try later, please close this modal window - press Cancel",
                  btnColor: "orangered",
                  bgColor: "red",
                  mainTextColor: "white",
                  headerBgColor: "orangered",
              };
        const {
            id,
            headerText,
            closeButton,
            text,
            btnColor,
            bgColor,
            mainTextColor,
            headerBgColor,
        } = findModalContent;
        return (
            <div
                className={active ? s.modal + " " + s.active : s.modal}
                onClick={() => setActive()}
            >
                <div
                    id={id}
                    className={active ? s["modal__content"] + " " + s.active : s["modal__content"]}
                    onClick={(e) => e.stopPropagation()}
                    style={{ color: mainTextColor, backgroundColor: bgColor }}
                >
                    {closeButton && (
                        <div className={s.x} onClick={() => setActive()}>
                            ×
                        </div>
                    )}
                    <header className={s.modalHeader} style={{ backgroundColor: headerBgColor }}>
                        {headerText}
                    </header>
                    <div className={s.modalBody}>{text}</div>
                    <Actions
                        extraClass={s["wrapper-btn"]}
                        btn1Bg={btnColor}
                        btn2Bg={btnColor}
                        onClick1={() => okSomeLogic()}
                        onClick2={() => setActive()}
                    />
                </div>
            </div>
        );
    }
}
