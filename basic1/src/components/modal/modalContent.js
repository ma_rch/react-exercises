export const modalsContent = [
    {
        id: "deleteConfirm1",
        headerText: "Do you want to delete this file?",
        closeButton: true,
        text: "Once you delete this file it won't be possible to undo this action. Are you sure you want to delete it?",
        bgColor: "red",
        mainTextColor: "white",
        btnColor: "darkred",
        headerBgColor: "darkred",
    },

    {
        id: "goodIdea1",
        headerText: "What about?",
        closeButton: false,
        text: "Look out the window, it's such a nice weather. Maybe go for a walk and have a cup of coffee?",
        bgColor: "green",
        mainTextColor: "white",
        btnColor: "darkgreen",
        headerBgColor: "darkgreen",
    },
];
