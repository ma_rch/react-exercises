import { Component } from "react";
import Button from "../button/index.jsx";
import s from "./actions.module.scss";

export default class Actions extends Component {
    static defaultProps = {
        btn1Text: "Ok",
        btn2Text: "Cancel",
    };
    render() {
        const {
            extraClass,
            btn1Text,
            btn1Bg,
            btn2Text,
            btn2Bg,
            onClick1,
            onClick2,
            btn1id,
            btn2id,
        } = this.props;
        const actionsClass = `${s.actions} ${extraClass ? extraClass : ""}`;

        return (
            <div className={actionsClass}>
                <Button id={btn1id} text={btn1Text} bg={btn1Bg} onClick={onClick1} />
                <Button id={btn2id} text={btn2Text} bg={btn2Bg} onClick={onClick2} />
            </div>
        );
    }
}
