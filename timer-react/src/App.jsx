import { useState, useEffect } from "react";

function App() {
    const [counter, setCounter] = useState(0);
    const [start, setStart] = useState(false);

    useEffect(() => {
        let intervalId = null;
        if (start) {
            intervalId = setInterval(() => {
                setCounter((prev) => prev + 1);
            }, 1000);
        }
        // return () => {
        //     clearInterval(intervalId);
        // };
    }, [start]);

    function startCounter() {
        setStart(true);
    }

    return (
        <div className="App">
            <div>
                <button className="btn" onClick={startCounter}>
                    Start
                </button>
                <button className="btn">Stop</button>
                <div className="counterContent">{counter}</div>
            </div>
        </div>
    );
}

export default App;
