import React, { Component } from "react";
import { Card } from "../card/Card.jsx";
import s from "./cards.module.scss";

export class CardsList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { cards, active, setActive, favoritCounter, setFavoritCounter } = this.props;
        return (
            <section className={s.cards}>
                {cards.map((card) => (
                    <Card
                        key={card.articul}
                        card={card}
                        active={active}
                        setActive={setActive}
                        favoritCounter={favoritCounter}
                        setFavoritCounter={setFavoritCounter}
                    />
                ))}
            </section>
        );
    }
}
