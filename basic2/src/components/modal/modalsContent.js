export function modalsContent() {
    return [
        {
            id: "modalAddToCart",
            headerText: "Подтверждение действия",
            bodyText: "Вы действительно хотите добавить этот товар в корзину ?",
        },

        {
            id: "modalAddToFavorit",
            headerText: "Подтверждение действия",
            bodyText: "Вы действительно хотите добавить этот товар в избранное ?",
        },
    ];
}
