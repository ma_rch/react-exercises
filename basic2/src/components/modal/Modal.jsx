import { Component } from "react";
import s from "./modal.module.scss";
import Button from "../button";
import { modalsContent } from "./modalsContent";

export class Modal extends Component {
    // constructor(props) {
    //     super(props);
    // }

    render() {
        const { active, setActive, okSomeLogic, btnAssignment } = this.props;
        let findModalContent = modalsContent().find((modal) => modal.id === btnAssignment);
        findModalContent = findModalContent
            ? findModalContent
            : {
                  headerText: "Error",
                  bodyText:
                      "no data, try later, please close this modal window - click any button in modal window",
              };
        const { id, headerText, bodyText } = findModalContent;

        return (
            <div
                className={active ? s.modal + " " + s.active : s.modal}
                onClick={() => setActive()}
            >
                <div
                    className={active ? s["modal__content"] + " " + s.active : s["modal__content"]}
                    onClick={(e) => e.stopPropagation()}
                    style={{ color: "bisque", backgroundColor: "brown" }}
                >
                    <div className={s.x} onClick={() => setActive()}>
                        ×
                    </div>
                    <header
                        className={s.modalHeader}
                        style={{ backgroundColor: "bisque", color: "brown" }}
                    >
                        {headerText}
                    </header>
                    <div className={s.modalBody}>{bodyText}</div>
                    <footer className={s.footer}>
                        <Button
                            text="OK"
                            onClick={id ? (event) => okSomeLogic(event) : () => setActive()}
                        />
                        <Button text="Cancel" onClick={() => setActive()} />
                    </footer>
                </div>
            </div>
        );
    }
}
