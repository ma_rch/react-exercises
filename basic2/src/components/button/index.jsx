import { Component } from "react";
import s from "./button.module.scss";

export default class Button extends Component {
    handleClick = (e) => {
        e.preventDefault(); // вызываем preventDefault() объекта event
        this.props.onClick(e); // передаем объект event в обработчик клика, который передали в пропсах
    };

    render() {
        const { text, bg, id, children, color, assignment } = this.props;
        return (
            <button
                id={id}
                className={s.btn}
                style={{ backgroundColor: bg, color }}
                onClick={this.handleClick} // передаем handleClick вместо onClick
                data-assignment={assignment}
            >
                {text}
                {children}
            </button>
        );
    }
}

Button.defaultProps = {
    color: "brown",
    bg: "bisque",
};
