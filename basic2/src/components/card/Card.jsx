import React, { Component } from "react";
import Button from "../button/index.jsx";
import s from "./card.module.scss";
import Star from "../star/Star.jsx";

export class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            favorit: false,
        };
    }

    clickOnFavorit = () => {
        const { card, favoritCounter, setFavoritCounter } = this.props;
        const favorits = favoritCounter || [];
        const index = favorits.findIndex((f) => f.articul === card.articul);

        if (index !== -1) {
            favorits.splice(index, 1);
            localStorage.setItem("favorits", JSON.stringify(favorits));
        } else {
            favorits.push(card);
            localStorage.setItem("favorits", JSON.stringify(favorits));
        }

        this.setState({ favorit: !this.state.favorit });
        setFavoritCounter();
    };

    componentDidMount() {
        const { card, favoritCounter } = this.props;
        const index = favoritCounter.findIndex((f) => f.articul === card.articul);

        if (index !== -1) {
            this.setState({ favorit: true });
        }
    }

    render() {
        const { name, price, img, articul, color } = this.props.card;
        const { active, setActive } = this.props;
        const { favorit } = this.state;
        return (
            <div className={s.card} id={articul} data-cards>
                <h2 className={s.text + " " + s.title}>{name}</h2>
                <img src={img} alt={name} width={280} />
                <div className={s.cardContent}>
                    <div>
                        <p className={s.text}>
                            <span className={s.prop}>Price:</span>
                            {price}
                        </p>
                        <p className={s.text}>
                            <span className={s.prop}>Articul:</span>
                            {articul}
                        </p>
                        <p className={s.text}>
                            <span className={s.prop}>Color:</span>
                            {color}
                        </p>
                    </div>
                    <div className={s.actions}>
                        <Button
                            text={"add to cart"}
                            bg={"brown"}
                            onClick={setActive}
                            color={"bisque"}
                            assignment={"modalAddToCart"}
                        />
                        <div className={s.wrapStar} onClick={this.clickOnFavorit}>
                            <Star fill={favorit ? "gold" : "white"} />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
