import { Component } from "react";
import s from "./header.module.scss";
import Star from "../star/Star";

export class Header extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { cartsCounter, favoritCounter } = this.props;
        return (
            <header className={s.header}>
                <h3 className={s.logo}>
                    Presents <span className={s.logo2}>Shop</span>{" "}
                </h3>
                <nav className={s.nav}>
                    <ul className={s["nav-list"]}>
                        <li className={s["nav-item"]}>Home</li>
                        <li className={s["nav-item"]}>About Us</li>
                        <li className={s["nav-item"]}>Contacts</li>
                    </ul>
                </nav>
                <div className={s.wrapper}>
                    <div className={s.star}>
                        <Star fill={"gold"} />
                        <span>{favoritCounter.length}</span>
                    </div>
                    <div className={s.cart}>
                        <img src="./img/shopping-cart.png" alt="" width={28} height={28} />
                        <span>{cartsCounter}</span>
                    </div>
                </div>
            </header>
        );
    }
}
