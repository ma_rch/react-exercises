import React, { Component } from "react";
import { CardsList } from "../components/cardsList/cardsList.jsx";

export class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { cards, active, setActive, favoritCounter, setFavoritCounter } = this.props;
        return (
            <>
                <CardsList
                    cards={cards}
                    active={active}
                    setActive={setActive}
                    favoritCounter={favoritCounter}
                    setFavoritCounter={setFavoritCounter}
                />
            </>
        );
    }
}
