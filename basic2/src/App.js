import React, { Component } from "react";
import { Home } from "./pages";
import { Header } from "./components/header";
import { Modal } from "./components/modal/Modal.jsx";

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: [],
            cartsCounter: parseInt(localStorage.getItem("cartsCounter")) || 0,
            cart: JSON.parse(localStorage.getItem("cart")) || [],
            favoritCounter: JSON.parse(localStorage.getItem("favorits")) || [],
            active: false,
            btnAssignment: null,
            selectedCard: {},
        };
    }

    componentDidMount() {
        fetch("./catalog.json")
            .then((res) => res.json())
            .then((data) => this.setState({ cards: data }));
    }

    setFavoritCounter = () => {
        this.setState({ favoritCounter: JSON.parse(localStorage.getItem("favorits")) || [] });
    };

    clickOnAddtoCart = (e) => {
        const { cart, selectedCard } = this.state;

        const isCardInCart = cart.some((card) => card.articul === selectedCard.articul);
        const newCartArr = isCardInCart
            ? cart.map((card) =>
                  card.articul === selectedCard.articul
                      ? { ...card, quantity: card.quantity + 1 }
                      : card
              )
            : [...cart, { ...selectedCard, quantity: 1 }];

        localStorage.setItem("cartsCounter", this.state.cartsCounter + 1);
        localStorage.setItem("cart", JSON.stringify(newCartArr));

        this.setState({ cartsCounter: this.state.cartsCounter + 1, cart: newCartArr });

        this.onCardClick();
    };

    onCardClick = (e) => {
        if (e && e.target.tagName === "BUTTON" && e.target.getAttribute("data-assignment")) {
            const id = e.target.closest("[data-cards]").id;
            this.setState({
                active: !this.state.active,
                btnAssignment: e.target.getAttribute("data-assignment"),
                selectedCard: this.state.cards.find((card) => card.articul + "" === id) || {},
            });
        } else {
            this.setState({ active: !this.state.active });
        }
        console.log(this.state.selectedCard);
    };

    render() {
        return (
            <>
                <Header
                    cartsCounter={this.state.cartsCounter}
                    favoritCounter={this.state.favoritCounter}
                />
                <Home
                    cards={this.state.cards}
                    active={this.state.active}
                    setActive={this.onCardClick}
                    favoritCounter={this.state.favoritCounter}
                    setFavoritCounter={this.setFavoritCounter}
                />
                <Modal
                    active={this.state.active}
                    setActive={this.onCardClick}
                    btnAssignment={this.state.btnAssignment}
                    okSomeLogic={this.clickOnAddtoCart}
                />
            </>
        );
    }
}

export default App;
