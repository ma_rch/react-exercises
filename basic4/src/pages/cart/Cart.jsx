import React from "react";
import { useSelector, useDispatch } from "react-redux";
import Button from "../../components/button/index.jsx";
import CardsList from "../../components/CardsList/CardsList.jsx";
import checkModalContent from "../../components/modal/checkModalContent.js";
import s from "./cart.module.scss";

const Cart = () => {
    const dispatch = useDispatch();
    const cart = useSelector((state) => state.cartCardsReducer.cartCards);
    const anyCardInCart = cart[0];
    return (
        <>
            {cart.length ? (
                <>
                    <div className={s.actions}>
                        <Button
                            text={"Clear All"}
                            bg={"bisque"}
                            color={"brown"}
                            onClick={(e) => {
                                checkModalContent(e, anyCardInCart, dispatch);
                            }}
                            assignment={"clearAllCart"}
                        ></Button>
                        <Button
                            text={"Сonfirm and pay"}
                            bg={"bisque"}
                            color={"brown"}
                            onClick={(e) => {
                                checkModalContent(e, anyCardInCart, dispatch);
                            }}
                            assignment={"pay"}
                        ></Button>
                    </div>
                    <CardsList
                        cards={cart}
                        btnAssignment={"removeFromCart"}
                        btnText={"remove from Cart"}
                    />
                </>
            ) : (
                <h2 className="h2Title" style={{ textAlign: "center", fontStyle: "italic" }}>
                    В корзине пока что ничего нет
                </h2>
            )}
        </>
    );
};

export default Cart;
