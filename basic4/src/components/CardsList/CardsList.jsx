import Card from "../Card/Card.jsx";
import s from "./cards.module.scss";

function CardsList(props) {
    const { btnAssignment, btnText, cards } = props;

    return (
        <section className={s.cards}>
            {cards.map((card) => (
                <Card
                    key={card.articul}
                    card={card}
                    btnAssignment={btnAssignment}
                    btnText={btnText}
                />
            ))}
        </section>
    );
}

export default CardsList;
