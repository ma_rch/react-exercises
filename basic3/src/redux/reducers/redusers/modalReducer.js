const initialState = {
    modal: false,
};

export function modalReducer(state = initialState, action) {
    switch (action.type) {
        case "MODAL_IS_VISIBLE":
            return { ...state, modal: !state.modal };
        default:
            return state;
    }
}
