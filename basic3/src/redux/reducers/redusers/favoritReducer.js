const initialState = {
    favoritCards: JSON.parse(localStorage.getItem("favoritsList")) || [],
};

export function favoritReducer(state = initialState, action) {
    switch (action.type) {
        case "FAVORIT":
            return {
                ...state,
                favoritCards: JSON.parse(localStorage.getItem("favoritsList")) || [],
            };
        default:
            return state;
    }
}
