const initialState = {
    cart: JSON.parse(localStorage.getItem("cart")) || [],
};

export function cartReducer(state = initialState, action) {
    switch (action.type) {
        case "CART":
            return {
                ...state,
                cart: JSON.parse(localStorage.getItem("cart")) || [],
            };
        default:
            return state;
    }
}
