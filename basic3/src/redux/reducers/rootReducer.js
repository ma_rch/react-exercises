import { combineReducers } from "redux";
import { modalReducer } from "./redusers/modalReducer.js";
import { favoritReducer } from "./redusers/favoritReducer.js";
import { selectedCardReducer } from "./redusers/SelectedCardReducer.js";
import { cartReducer } from "./redusers/cartReducer.js";

const rootReducer = combineReducers({
    modalReducer,
    favoritReducer,
    selectedCardReducer,
    cartReducer,
});

export default rootReducer;
