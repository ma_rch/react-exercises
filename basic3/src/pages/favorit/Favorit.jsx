import React from "react";
import { useSelector, useDispatch } from "react-redux";
import CardsList from "../../components/CardsList/CardsList.jsx";
import Button from "../../components/button/index.jsx";
import checkModalContent from "../../components/modal/checkModalContent.js";

const Favorit = () => {
    const dispatch = useDispatch();
    const favorit = useSelector((state) => state.favoritReducer.favoritCards);
    const anyCardInFavorit = favorit[0];

    return (
        <>
            {favorit.length ? (
                <>
                    <div style={{ marginLeft: "10px", marginTop: "10px" }}>
                        <Button
                            text={"Clear All"}
                            bg={"bisque"}
                            color={"brown"}
                            onClick={(e) => {
                                checkModalContent(e, anyCardInFavorit, dispatch);
                            }}
                            assignment={"clearAllFavorits"}
                        ></Button>
                    </div>
                    <CardsList
                        cards={favorit}
                        btnAssignment={"modalAddToCart"}
                        btnText={"add to Cart"}
                    />
                </>
            ) : (
                <h2 className="h2Title" style={{ textAlign: "center", fontStyle: "italic" }}>
                    В избранном пока что ничего нет
                </h2>
            )}
        </>
    );
};

export default Favorit;
