import React, { useState, useEffect } from "react";
import CardsList from "../../components/CardsList/CardsList";

const Home = (props) => {
    const [cards, setCards] = useState([]);

    function getCards() {
        return fetch("./catalog.json")
            .then((res) => res.json())
            .then((data) => setCards(data));
    }

    useEffect(() => {
        getCards();
    }, []);

    return (
        <>
            <CardsList
                cards={cards}
                btnAssignment={"modalAddToCart"}
                btnText={"add to Cart"}
            ></CardsList>
        </>
    );
};

export default Home;
