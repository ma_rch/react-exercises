import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import s from "./card.module.scss";
import Button from "../button";
import Star from "../Star/Star.jsx";
import { favoritAction } from "../../redux/actions/allActions.js";
import checkModalContent from "../modal/checkModalContent";

function Card(props) {
    const dispatch = useDispatch();
    const { name, price, img, articul, color, quantity, sum } = props.card;
    const { btnText, btnAssignment } = props;

    const favorit = useSelector((state) => state.favoritReducer.favoritCards);

    const [starIsFavorit, setStarIsFavorit] = useState(false);

    const setStar = () => setStarIsFavorit((prevState) => !prevState);

    useEffect(() => {
        const index = favorit.findIndex((f) => f.articul === props.card.articul);
        if (index !== -1) {
            setStarIsFavorit(true);
        }
    }, [favorit, props.card]);

    const clickOnStar = () => {
        const index = favorit.findIndex((f) => f.articul === props.card.articul);
        const newFavorit =
            index === -1 ? [...favorit, props.card] : favorit.filter((f, i) => i !== index);
        localStorage.setItem("favoritsList", JSON.stringify(newFavorit));
        dispatch(favoritAction());
        setStar();
    };

    return (
        <div className={s.card} id={articul} data-cards>
            <h2 className={s.text + " " + s.title}>{name}</h2>
            <img src={img} alt={name} width={280} />
            <div className={s.cardContent}>
                <div>
                    <p className={s.text}>
                        <span className={s.prop}>Price:</span>
                        {price}
                    </p>
                    <p className={s.text}>
                        <span className={s.prop}>Articul:</span>
                        {articul}
                    </p>
                    <p className={s.text}>
                        <span className={s.prop}>Color:</span>
                        {color}
                    </p>
                    {quantity && sum && (
                        <>
                            <p>
                                <span className={s.prop}>Quantity:</span>
                                {quantity}
                            </p>
                            <p>
                                <span className={s.prop}>Sum:</span>
                                {sum}
                            </p>
                        </>
                    )}
                </div>
                <div className={s.actions}>
                    <Button
                        text={btnText}
                        assignment={btnAssignment}
                        onClick={(e) => {
                            checkModalContent(e, props.card, dispatch);
                        }}
                    />
                    <div className={s.wrapStar} onClick={clickOnStar}>
                        <Star fill={starIsFavorit ? "gold" : "bisque"} />
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Card;
