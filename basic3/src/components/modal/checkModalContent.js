import { modalAction, selectedCardAction } from "../../redux/actions/allActions.js";

function checkModalContent(e, card, dispatch) {
    if (e && e.target.tagName === "BUTTON") {
        const btnDataAttr = e.target.getAttribute("data-assignment");
        dispatch(selectedCardAction(btnDataAttr, card));
        dispatch(modalAction());
    } else {
        dispatch(modalAction());
    }
}

export default checkModalContent;
