import { cartAction, modalAction, favoritAction } from "../../../redux/actions/allActions.js";

export function addToCart(selectedCard, cart, dispatch) {
    const isCardInCart = cart.some((card) => card.articul === selectedCard.articul);
    const newCartArr = isCardInCart
        ? cart.map((card) =>
              card.articul === selectedCard.articul
                  ? {
                        ...card,
                        quantity: card.quantity + 1,
                        sum: (card.quantity + 1) * card.price,
                    }
                  : card
          )
        : [...cart, { ...selectedCard, quantity: 1, sum: selectedCard.price }];

    localStorage.setItem("cart", JSON.stringify(newCartArr));
    dispatch(cartAction());
    dispatch(modalAction());
}

export function removeFromCart(card, cart, dispatch) {
    const newCart = cart.filter((el) => el.articul !== card.articul);
    localStorage.setItem("cart", JSON.stringify(newCart));
    dispatch(cartAction());
    dispatch(modalAction());
}

export function clearAllCart(card, cart, dispatch) {
    localStorage.setItem("cart", "[]");
    dispatch(cartAction([]));
    dispatch(modalAction());
}

export function clearAllFavorits(card, cart, dispatch) {
    localStorage.setItem("favoritsList", "[]");
    dispatch(favoritAction([])); // обновление состояния
    dispatch(modalAction());
}
